#!/bin/sh
set -e

# Prepend "ddl-userman" if the first argument is not an executable
if ! type -- "$1" &> /dev/null; then
	set -- ddl-userman "$@"
fi

exec "$@"
