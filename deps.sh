#!/bin/bash

if [ "x${GOPATH}" != "x$(pwd)/vendor:$(pwd)" ]; then
	echo "Set GOPATH to $(pwd)/vendor:$(pwd)"
	exit 1
fi

VGOPATH="$(pwd)/vendor"

if [ -d "${VGOPATH}/src" ]; then
	echo "Vendor is populated"
	exit 0
fi

go get github.com/dgrijalva/jwt-go
go get github.com/gorilla/mux
go get github.com/gorilla/websocket
go get github.com/streadway/amqp
go get gopkg.in/mgo.v2
go get gopkg.in/yaml.v2
go get golang.org/x/crypto/bcrypt
