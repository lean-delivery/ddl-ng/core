package main

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Render struct which contains
// an array of descriptions
type Render struct {
	Render []description `json:"description"`
}

// Description struct which contains a data
type description struct {
	ID       uint   `json:"id"`
	Name     string `json:"name"`
	Endpoint string `json:"endpoint"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

const clustersCollection = "Clusters"

// Get a new Session to MongoDB. Do not need to cache it.
// mgo has an internal pool.
func (c *Config) getSession() (*mgo.Session, error) {

	session, err := mgo.Dial(c.Mongo.Host)
	if err != nil {
		return nil, err
	}

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	return session, nil
}

// Try to Get the MongoDB and yeah save it also.
func (c *Config) getDb(s *mgo.Session) (*mgo.Database, error) {
	db := s.DB(config.Mongo.DB)

	if len(config.Mongo.Username) == 0 {
		if len(config.Mongo.Password) == 0 {
			return db, nil
		}
		log.Fatal("Username and Password required for MongoDB connectivity")
	}

	if err := db.Login(config.Mongo.Username, config.Mongo.Password); err != nil {
		return nil, err
	}

	return db, nil
}

// Get the description for a clusterID saved in the database.
func (c *Config) Get(clusterID string) ([]byte, error) {
	log.Infof("Getting description for %s not inplemented", clusterID)
	var out []byte
	return out, nil
}

// Save the state to a database, even if the same version/serial exists, new
// entry will be created. This will help maintain versions and never loose
// history.
func (c *Config) Save(clusterID string, data Render) error {
	log.Infof("Saving description %v", clusterID)

	session, err := c.getSession()
	if err != nil {
		return err
	}

	defer session.Close()

	db, err := c.getDb(session)
	if err != nil {
		return err
	}
	colQuerier := bson.M{"_id": bson.ObjectIdHex(clusterID)}
	change := bson.M{"$set": bson.M{"description": data.Render}}
	err = db.C(clustersCollection).Update(colQuerier, change)
	if err != nil {
		log.Errorf("Cannot update description for cluster %s: %s", clusterID, err.Error())
		return err
	}
	log.Infof("Description successfully updated for cluster %s", clusterID)
	return nil
}

// Delete the state from database? How do we care. Just archive the collection
// by renaming it to <coll>-archive-<timestamp>.
func (c *Config) Delete(clusterID string) error {
	log.Infof("Deleting description for %s is not implemented", clusterID)
	return nil
}
