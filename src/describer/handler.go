package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

const (
	ok            = 200
	internalError = 500
)

// Describer, the web API to update description
func describer(w http.ResponseWriter, r *http.Request) {

	clusterID := strings.TrimPrefix(strings.TrimSuffix(r.URL.Path, "/"), "/")

	switch r.Method {
	case "GET":
		descr, err := config.Get(clusterID)
		if err != nil {
			log.Errorf("Error Getting Description %v", err)
			w.WriteHeader(internalError)
		}
		if _, err := w.Write(descr); err != nil {
			log.Errorf("Error Writing Description: %v", err)
		}
	case "POST":
		decoder := json.NewDecoder(r.Body)
		var clDescr Render
		if err := decoder.Decode(&clDescr); err != nil {
			log.Errorf("Cannot decode request body for cluster %s: %s", clusterID, err.Error())
			w.WriteHeader(internalError)
			panic(err)
		} else if err := config.Save(clusterID, clDescr); err != nil {
			log.Errorf("Error Saving Description: %v", err)
		}
	case "DELETE":
		if err := config.Delete(clusterID); err != nil {
			w.WriteHeader(ok)
		} else {
			log.Errorf("Error Deleting Description: %v", err)
			w.WriteHeader(internalError)
		}
	default:
		w.WriteHeader(internalError)
		if _, err := w.Write([]byte(fmt.Sprintf("Unknown method: %s", r.Method))); err != nil {
			log.Errorf("Error Writing response: %v", err)
		}
	}
}
