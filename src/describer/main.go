package main

import (
	"net/http"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// Config is global configuration for describer
type Config struct {
	Listen  string `mapstructure:"listen"`
	Port    string `mapstructure:"port"`
	Mongo   mongo
	Logging logger
}

type mongo struct {
	Host     string `mapstructure:"host"`
	DB       string `mapstructure:"db"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
}

type logger struct {
	Format    string `mapstructure:"format"`
	Level     string `mapstructure:"level"`
	Timestamp bool   `mapstructure:"timestamp"`
}

// Global config
var config Config
var log = logrus.New()

func (config *Config) setLogging() {

	// inverse timestamp
	var dts bool
	if config.Logging.Timestamp {
		dts = false
	} else {
		dts = true
	}

	if config.Logging.Format == "" || config.Logging.Format == "text" {
		log.Formatter = &logrus.TextFormatter{FullTimestamp: true, DisableTimestamp: dts}
	} else {
		log.Formatter = &logrus.JSONFormatter{DisableTimestamp: dts}
	}

	switch config.Logging.Level {
	case "info":
		log.SetLevel(logrus.InfoLevel)
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "warn":
		log.SetLevel(logrus.WarnLevel)
	case "error":
		log.SetLevel(logrus.ErrorLevel)
	default:
		log.SetLevel(logrus.InfoLevel)
	}
	// file or stdout
	log.Out = os.Stdout
}

func main() {
	// Setup config
	viper.SetConfigName("describer")
	viper.AddConfigPath("/etc/ddl")
	viper.AddConfigPath(".")
	// Logging
	viper.SetDefault("logging.format", "text")
	viper.SetDefault("logging.level", "info")
	viper.SetDefault("logging.timestamp", true)
	// Mongo
	viper.SetDefault("mongo.host", "localhost:27017")
	viper.SetDefault("mongo.db", "ddl")
	viper.SetDefault("mongo.username", "mongouser")
	viper.SetDefault("mongo.password", "mongopass")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read config file: %v", err)
	}

	envVarPrefix := "describer"
	viper.SetEnvPrefix(envVarPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Failed to setup configuration: %v", err)
	}
	config.setLogging()

	log.Infof("Starting describer server listening on %s:%s", config.Listen, config.Port)
	http.HandleFunc("/", describer)
	if err := http.ListenAndServe(config.Listen+":"+config.Port, nil); err != nil {
		log.Fatalf("Unable to start HTTP listener: %s", err.Error())
	}
}
