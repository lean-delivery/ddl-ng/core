package main

import (
	"api"
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2"
	"os"
	"strings"
)

type logger struct {
	Format    string `mapstructure:"format"`
	Level     string `mapstructure:"level"`
	Timestamp bool   `mapstructure:"timestamp"`
}

type Config struct {
	DbConnHost string
	DbConnDb string
	DbConnUsername string
	DbConnPassword string

	UserLogin string
	UserPassword string

	Logging logger
}

// Global config
var config Config
var log = logrus.New()

func (config *Config) setLogging() {

	// inverse timestamp
	var dts bool
	if config.Logging.Timestamp {
		dts = false
	} else {
		dts = true
	}

	if config.Logging.Format == "" || config.Logging.Format == "text" {
		log.Formatter = &logrus.TextFormatter{FullTimestamp: true, DisableTimestamp: dts}
	} else {
		log.Formatter = &logrus.JSONFormatter{DisableTimestamp: dts}
	}

	switch config.Logging.Level {
	case "info":
		log.SetLevel(logrus.InfoLevel)
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "warn":
		log.SetLevel(logrus.WarnLevel)
	case "error":
		log.SetLevel(logrus.ErrorLevel)
	default:
		log.SetLevel(logrus.InfoLevel)
	}
	// file or stdout
	log.Out = os.Stdout
}

func main() {
	// Setup config
	viper.SetConfigName("userman")
	viper.AddConfigPath("/etc/ddl")
	viper.AddConfigPath(".")

	pflag.String("dbConnHost", "localhost:27017", "mongo host:port")
	pflag.String("dbConnDb", "ansaaas", "mongo database")
	pflag.String("dbConnUsername", "ansaas-user-1", "mongo login")
	pflag.String("dbConnPassword", "pass", "mongo password")

	pflag.String("userLogin", "ansaas-uiuser-1", "regular user name")
	pflag.String("userPassword", "pass", "regular user password")

	pflag.String("loggingFormat", "text", "logging format")
	pflag.String("loggingLevel", "info", "logging level")
	pflag.Bool("loggingTimestamp", true, "logging timestamp true / false")

	pflag.Parse()

	if err := viper.BindPFlag("logging.format", pflag.Lookup("loggingFormat")); err != nil{
		log.Warnf("Failed to parse command line flags: %v", err)
	}
	if err := viper.BindPFlag("logging.level", pflag.Lookup("loggingLevel")); err != nil {
		log.Warnf("Failed to parse command line flags: %v", err)
	}
	if err := viper.BindPFlag("logging.timestamp", pflag.Lookup("loggingTimestamp")); err != nil {
		log.Warnf("Failed to parse command line flags: %v", err)
	}

	if err := viper.BindPFlags(pflag.CommandLine); err != nil{
		log.Warnf("Failed to parse command line flags: %v", err)
	}

	envVarPrefix := "userman"
	viper.SetEnvPrefix(envVarPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Failed to setup configuration: %v", err)
	}
	config.setLogging()

	hash, err := bcrypt.GenerateFromPassword([]byte(config.UserPassword), 0)
	if err != nil {
		log.Errorf("Cannot hash password: %s", err.Error())
		return
	}

	info := mgo.DialInfo{
		Addrs:    []string{config.DbConnHost},
		Database: config.DbConnDb,
		Username: config.DbConnUsername,
		Password: config.DbConnPassword,
	}

	s, err := mgo.DialWithInfo(&info)
	if err != nil {
		log.Fatalf("Cannot connect to db: %s", err.Error())
		return
	}

	err = s.DB(config.DbConnDb).C(api.DBColUsers).Insert(
		&api.UserEntry{Name: config.DbConnUsername, PassHash: hash},
	)
	if err != nil {
		log.Fatalf("Cannot insert user: %s", err.Error())
		return
	}
}
