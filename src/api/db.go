package api

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

const (
	DBColUsers    = "Users"
	DBColClusters = "Clusters"
	DBColJobs     = "Jobs"
)

type JobLogEntry struct {
	Seq  int
	Ts   time.Time
	Type string
	Text string
}

type UserEntry struct {
	ObjID    bson.ObjectId `bson:"_id,omitempty"`
	Name     string        `bson:"name"`
	PassHash []byte        `bson:"pass_hash"`
}
