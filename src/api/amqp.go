package api

const (
	WorkerQueue   = "ansaas_worker"
	AbortExchange = "ansaas_abort"
	LogsExchange  = "ansaas_wlogs"

	LogFinType = "fin"
)

type JobMsg struct {
	JobID      string `json:"jobid"`
	User       string `json:"user"`
	Playbook   string `json:"playbook"`
	Entrypoint string `json:"entrypoint"`
	ClusterId  string `json:"clusterid"`
	// IPs		[]string	`json:"ips"`
	// Keys		string		`json:"keys"`
	// Hosts		string		`json:"hosts,omitempty"`
}

type AbortMsg struct {
	JobID string `json:"jobid"`
}
