package api

const (
	DbError      uint = 4 // Error requesting database (except NotFound)
	Duplicate    uint = 5 // ID duplication
	NotFound     uint = 6 // No resource found
	NotAvailable uint = 7 // Operation not possible on this object
)

const (
	JobStarted   string = "started"
	JobSucceeded string = "succeeded"
	JobFailed    string = "failed"
	JobAborted   string = "aborted"
)

type UserLogin struct {
	Name string `json:"username"`
	Pass string `json:"password"`
}

type ClusterAdd struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type ClusterDelById struct {
	DoRemoveDbEntry bool `json:"doRemoveDbEntry"`
}

type ClusterInfo struct {
	ID          string                    `json:"id"`
	Name        string                    `json:"name"`
	LD          *ClusterJobInfo           `json:"last_job,omitempty"`
	Nodes       []*NodeInfo               `json:"nodes,omitempty"`
	Description []*ClusterDescriptionInfo `json:"description"`
}

type ClusterDescriptionInfo struct {
	ID       uint   `json:"id"`
	Name     string `json:"name"`
	Endpoint string `json:"endpoint"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

type NodeAdd struct {
	Name string `json:"name"`
	Addr string `json:"addr"`
	Port uint   `json:"port,omitempty"`
}

type NodeInfo struct {
	Name string `json:"name"`
	Addr string `json:"addr"`
	Port uint   `json:"port,omitempty"`
}

type ClusterKeyInfo struct {
	Key string `json:"key,omitempty"`
}

type ClusterJob struct {
	Playbook string `json:"playbook"`
	Hosts    string `json:"hosts_ini_base64"`
}

type ClusterJobInfo struct {
	Id       string `json:"id"`
	Playbook string `json:"playbook"`
	Started  string `json:"started"`
	Status   string `json:"status"`
	Finished string `json:"finished,omitempty"`
}

type LogRecord struct {
	Ts   string `json:"ts"`
	Type string `json:"type"`
	Text string `json:"text"`
}
