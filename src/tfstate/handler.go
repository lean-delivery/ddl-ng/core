package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"strings"
)

const (
	ok            = 200
	internalError = 500
)

// Terra State, the web server
func terraState(w http.ResponseWriter, r *http.Request) {

	ident := strings.TrimPrefix(strings.TrimSuffix(r.URL.Path, "/"), "/")

	switch r.Method {
	case "GET":
		state, err := config.Get(ident)
		if err != nil {
			log.Fatalf("Error Getting State %v", err)
			w.WriteHeader(internalError)
		}
		if _, err := w.Write(state); err != nil{
			log.Errorf("Error Writing response: %v", err)
		}
	case "POST":
		buf := new(bytes.Buffer)
		if _, err := io.Copy(buf, r.Body); err != nil {
			w.WriteHeader(internalError)
		} else if err := config.Save(ident, buf.Bytes()); err != nil {
			log.Fatalf("Error Saving State: %v", err)
		}
	case "DELETE":
		if err := config.Delete(ident); err != nil {
			w.WriteHeader(ok)
		} else {
			log.Fatalf("Error Deleting State: %v", err)
			w.WriteHeader(internalError)
		}
	default:
		w.WriteHeader(internalError)
		if _, err := w.Write([]byte(fmt.Sprintf("Unknown method: %s", r.Method))); err != nil {
			log.Errorf("Error Writing response: %v", err)
		}
	}
}
