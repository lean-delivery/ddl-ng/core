# tfstate
Restful Terraform remote state server

Refer to the Documentation [here](https://www.terraform.io/docs/state/remote/http.html) on how to setup your terraform to talk to a resftful server
Currently this only supports saving the State to MongoDB after the Restful server receives it, but you can have more implementations.
Look at mongo.go for a Sample implementation and storeage.go is the basic interface that every engine needs to implement.

Make sure your GOPATH and all is set.

### Sample Configuration file

```yaml
---
listen: 127.0.0.1
port: 12345
logging:
  format: text
  output: stdout
  level: info
  timestamp: true
mongo:
  host: localhost:27017
  db: terraform
  username: terraform
  password: terraform 
```
