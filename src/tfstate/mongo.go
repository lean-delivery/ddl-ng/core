package main

import (
	"encoding/json"

	"gopkg.in/mgo.v2"
)

type tfState struct {
	Version uint32
	Serial  uint32
	Modules []map[string]interface{}
}

// Get a new Session to MongoDB. Do not need to cache it.
// mgo has an internal pool.
func (c *Config) getSession() (*mgo.Session, error) {

	session, err := mgo.Dial(c.Mongo.Host)
	if err != nil {
		return nil, err
	}

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	return session, nil
}

// Try to Get the MongoDB and yeah save it also.
func (c *Config) getDb(s *mgo.Session) (*mgo.Database, error) {
	db := s.DB(config.Mongo.DB)

	if len(config.Mongo.Username) == 0 {
		if len(config.Mongo.Password) == 0 {
			return db, nil
		}
		log.Fatal("Username and Password required for MongoDB connectivity")
	}

	if err := db.Login(config.Mongo.Username, config.Mongo.Password); err != nil {
		return nil, err
	}

	return db, nil
}

// Get the latest state for a ident saved in the database.
func (c *Config) Get(ident string) ([]byte, error) {
	log.Println("Getting ident", ident)

	session, err := c.getSession()
	if err != nil {
		return nil, err
	}

	defer session.Close()

	log.Debugf("Debug Mode %v", session)

	db, err := c.getDb(session)
	if err != nil {
		return nil, err
	}

	coll := db.C(ident)

	state := &tfState{}
	// Dirty hack. To have default state v3 if we have no states in mongo
	// https://github.com/hashicorp/terraform/blob/148f6be244ac603f51c90c06a6be0dfe07abd71a/terraform/state.go#L1972
	if state.Version == 0 {
		state.Version = 3
	}
	if err := coll.Find(nil).Sort("-$natural").One(&state); err != nil {
		if err.Error() != "not found" {
			return nil, err
		}
	}
	out, err := json.Marshal(state)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Save the state to a database, even if the same version/serial exists, new
// entry will be created. This will help maintain versions and never loose
// history.
func (c *Config) Save(ident string, data []byte) error {
	log.Infof("Saving ident %v", ident)

	m := &tfState{}
	if err := json.Unmarshal(data, m); err != nil {
		return err
	}

	session, err := c.getSession()
	if err != nil {
		return err
	}

	defer session.Close()

	db, err := c.getDb(session)
	if err != nil {
		return err
	}

	coll := db.C(ident)

	return coll.Insert(m)
}

// Delete the state from database? How do we care. Just archive the collection
// by renaming it to <coll>-archive-<timestamp>.
func (c *Config) Delete(ident string) error {
	log.Println("Deleting ident", ident)
	return nil
}
