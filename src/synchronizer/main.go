package main

import (
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	transportHTTP "gopkg.in/src-d/go-git.v4/plumbing/transport/http"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"
)

type Config struct {
	Listen     string `mapstructure:"listen"`
	Port       string `mapstructure:"port"`
	Logging    logger
	LastUpdate time.Time
	Repo       repo
}

type logger struct {
	Format    string `mapstructure:"format"`
	Level     string `mapstructure:"level"`
	Timestamp bool   `mapstructure:"timestamp"`
}

type repo struct {
	URL           string `mapstructure:"url"`
	Path          string `mapstructure:"path"`
	Directory     string `mapstructure:"directory"`
	Branch        string `mapstructure:"branch"`
	Mode          string `mapstructure:"mode"`
	Login         string `mapstructure:"login"`
	Password      string `mapstructure:"password"`
	Remote        string `mapstructure:"remote"`
	Secret        string `mapstructure:"secret"`
	SSHPrivKey    string `mapstructure:"sshPrivKey"`
	SSHPassPhrase string `mapstructure:"sshPassPhrase"`
	SSHInVar      bool   `mapstructure:"sshInVar"`
	Trigger       string `mapstructure:"trigger"`
	Wipe          bool   `mapstructure:"wipe"`
}

// C is global config
var config Config
var log = logrus.New()
var repoClone func(r *repo) error
var repoUpdateOptions func(r *repo) *git.FetchOptions

func (config *Config) FindRepo(path string) bool {
	if config.Repo.Path == cleanURL(path) {
		return true
	}
	return false
}

func cleanURL(url string) string {
	// strip trailing slash
	if url[len(url)-1] == '/' {
		return url[:len(url)-1]
	}
	return url
}
func modeSSHClone(r *repo) error {
	var sshAuth *ssh.PublicKeys
	var err error
	if r.SSHInVar {
		sshAuth, err = ssh.NewPublicKeys("git", []byte(r.SSHPrivKey), r.SSHPassPhrase)
	} else {
		sshAuth, err = ssh.NewPublicKeysFromFile("git", r.SSHPrivKey, r.SSHPassPhrase)
	}
	if err != nil {
		log.Errorf("Failed to setup ssh auth: %v", err)
		return nil
	}

	_, err = git.PlainClone(r.Directory, false, &git.CloneOptions{
		URL:           r.URL,
		ReferenceName: plumbing.ReferenceName("refs/heads/" + r.Branch),
		SingleBranch:  true,
		Auth:          sshAuth,
	})
	return err
}
func modeTokenClone(r *repo) error {
	_, err := git.PlainClone(r.Directory, false, &git.CloneOptions{
		URL:           r.URL,
		ReferenceName: plumbing.ReferenceName("refs/heads/" + r.Branch),
		SingleBranch:  true,
		Auth: &transportHTTP.BasicAuth{
			Username: r.Login,
			Password: r.Password,
		},
	})
	return err
}

func (r *repo) clone() {

	log.WithFields(logrus.Fields{
		"repo":   r.Name(),
		"path":   r.Path,
		"branch": r.Branch,
	})
	// clone here
	err := repoClone(r)
	if err != nil {
		log.Errorf("Failed to clone repository: %v", err)
		return
	}
	log.Infof("Cloned repository: %s", r.Name())

	r.touchTrigger()
}

func modeSSHUpldate(r *repo) *git.FetchOptions {
	sshAuth, err := ssh.NewPublicKeysFromFile("git", r.SSHPrivKey, r.SSHPassPhrase)
	if err != nil {
		log.Errorf("Failed to setup ssh auth: %v", err)
	}
	options := &git.FetchOptions{
		RemoteName: r.Remote,
		Auth:       sshAuth,
	}
	return options
}

func modeTokenUpdate(r *repo) *git.FetchOptions {
	options := &git.FetchOptions{
		RemoteName: r.Remote,
		Auth: &transportHTTP.BasicAuth{
			Username: r.Login,
			Password: r.Password,
		},
	}
	return options
}

// essentially git fetch and git reset --hard origin/master | latest remote commit
func (r *repo) update() {
	rlog := log.WithFields(logrus.Fields{
		"repo":   r.Name(),
		"path":   r.Path,
		"branch": r.Branch,
		"remote": r.Remote,
	})

	repo, err := git.PlainOpen(r.Directory)
	if err != nil {
		rlog.Errorf("Failed to open local git repository: %v", err)
		return
	}

	w, err := repo.Worktree()
	if err != nil {
		rlog.Errorf("Failed to open work tree for repository: %v", err)
		return
	}
	err = repo.Fetch(repoUpdateOptions(r))

	// git reset --hard [origin/master|hash] local changes
	// wipe: true
	if r.Wipe {
		localStatus, _ := w.Status()
		if !localStatus.IsClean() {
			wipeLocalRef, err := repo.Reference(plumbing.ReferenceName("HEAD"), true)
			err = w.Reset(&git.ResetOptions{Mode: git.HardReset, Commit: wipeLocalRef.Hash()})
			if err != nil {
				rlog.Errorf("Failed to wipe working directory: %v", err)
				return
			}
			rlog.Infof("Changes in working dir %v is wiped", r.Directory)
		}
	}

	if err == git.NoErrAlreadyUpToDate {
		rlog.Info("No new commits")
		return
	}
	if err != nil {
		rlog.Errorf("Failed to fetch updates: %v", err)
		return
	}
	rlog.Info("Fetched new updates")

	// Get local and remote refs to compare hashes before we proceed
	remoteRef, err := repo.Reference(plumbing.ReferenceName("refs/remotes/"+r.Remote+"/"+r.Branch), true)
	if err != nil {
		rlog.Errorf("Failed to get remote reference for remotes/%s/%s: %v", r.Remote, r.Branch, err)
		return
	}
	localRef, err := repo.Reference(plumbing.ReferenceName("HEAD"), true)
	if err != nil {
		rlog.Errorf("Failed to get local reference for HEAD: %v", err)
		return
	}

	if remoteRef.Hash() == localRef.Hash() {
		rlog.Warning("Already up to date")
		return
	}

	// git reset --hard [origin/master|hash]
	err = w.Reset(&git.ResetOptions{Mode: git.HardReset, Commit: remoteRef.Hash()})
	if err != nil {
		rlog.Errorf("Failed to hard reset work tree: %v", err)
		return
	}
	rlog.Info("Hard reset successful, confirming changes....")
	headRef, err := repo.Reference(plumbing.ReferenceName("HEAD"), true)
	if err != nil {
		rlog.Errorf("Failed to get local HEAD reference: %v", err)
		return
	}

	if headRef.Hash() == remoteRef.Hash() {
		rlog.Infof("Changes confirmed, latest hash: %v", headRef.Hash())
	} else {
		rlog.Error("Something went wrong, hashes don't match!")
		rlog.Debugf("Remote hash: %v", remoteRef.Hash())
		rlog.Debugf("Local hash:  %v", headRef.Hash())
		return
	}

	r.touchTrigger()
}

func (r *repo) touchTrigger() {
	if r.HasTrigger() {
		if err := os.Chtimes(r.Trigger, time.Now(), time.Now()); err != nil {
			log.Errorf("Failed to update trigger file: %v", err)
			return
		}
		log.Info("Successfully updated trigger file")
	}
}

// short name for the logs
func (r *repo) Name() string {
	return strings.TrimSuffix((strings.TrimPrefix(r.URL, "git@github.com:")), ".git")
}

func isEmpty(field string) bool {
	if len(field) == 0 {
		return true
	}
	return false
}

func (r *repo) HasTrigger() bool {
	if isEmpty(r.Trigger) {
		return false
	}
	return true
}

func (r *repo) HasSecret() bool {
	if isEmpty(r.Secret) {
		return false
	}
	return true
}

func handler(w http.ResponseWriter, r *http.Request) {
	ok := config.FindRepo(r.URL.Path)
	if !ok {
		log.Warnf("Repository not found for path: %v", r.URL.Path)
		return
	}

	if _, err := os.Stat(config.Repo.Directory); err != nil {
		go config.Repo.clone()
	} else {
		go config.Repo.update()
	}
}

func (config *Config) setRepoDefaults() {
	if config.Repo.Branch == "" {
		config.Repo.Branch = "master"
	}
	if config.Repo.Remote == "" {
		config.Repo.Remote = "origin"
	}
}

func (config *Config) setLogging() {

	// inverse timestamp
	var dts bool
	if config.Logging.Timestamp {
		dts = false
	} else {
		dts = true
	}

	if config.Logging.Format == "" || config.Logging.Format == "text" {
		log.Formatter = &logrus.TextFormatter{FullTimestamp: true, DisableTimestamp: dts}
	} else {
		log.Formatter = &logrus.JSONFormatter{DisableTimestamp: dts}
	}

	switch config.Logging.Level {
	case "info":
		log.SetLevel(logrus.InfoLevel)
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "warn":
		log.SetLevel(logrus.WarnLevel)
	case "error":
		log.SetLevel(logrus.ErrorLevel)
	default:
		log.SetLevel(logrus.InfoLevel)
	}
	// file or stdout
	log.Out = os.Stdout
}

func (config *Config) refreshTasks() {
	config.setLogging()
	config.setRepoDefaults()
	config.LastUpdate = time.Now()
}

func main() {
	// setup config
	viper.SetConfigName("synchronizer")
	viper.AddConfigPath("/etc/ddl")
	viper.AddConfigPath(".")
	viper.SetDefault("listen", "127.0.0.1")
	viper.SetDefault("port", 8951)
	viper.SetDefault("logging.format", "text")
	viper.SetDefault("logging.output", "stdout")
	viper.SetDefault("logging.level", "info")
	viper.SetDefault("logging.timestamp", true)
	viper.SetDefault("repo.url", "https://gitlab.com:lean-delivery/ddl-ng/ddl-example")
	viper.SetDefault("repo.path", "/ddl/library")
	viper.SetDefault("repo.directory", "/var/ddl")
	viper.SetDefault("repo.branch", "master")
	viper.SetDefault("repo.mode", "ssh")
	viper.SetDefault("repo.login", "git")
	viper.SetDefault("repo.password", "empty")
	viper.SetDefault("repo.remote", "origin")
	viper.SetDefault("repo.trigger", "")
	viper.SetDefault("repo.secret", "")
	viper.SetDefault("repo.sshPrivKey", "/path/to/private/id_rsa")
	viper.SetDefault("repo.sshInVar", false)
	viper.SetDefault("repo.sshPassPhrase", "")
	viper.SetDefault("repo.wipe", false)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read config file: %v", err)
	}
	envVarPrefix := "synchronizer"
	viper.SetEnvPrefix(envVarPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Failed to setup configuration: %v", err)
	}

	config.refreshTasks()
	//
	switch config.Repo.Mode {
   	case "token":
		repoClone = modeTokenClone
		repoUpdateOptions = modeTokenUpdate
	default:
		repoClone = modeSSHClone
		repoUpdateOptions = modeSSHUpldate
	}

	// Start the server.
	// (listen and port changes require a restart)
	http.HandleFunc("/", handler)
	log.Printf("Starting http server %s:%s", config.Listen, config.Port)
	err := http.ListenAndServe(config.Listen+":"+config.Port, nil)
	if err != nil {
		log.Printf("ListenAndServe error: %s", err.Error())
	}
}
