package main

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Get a new Session to MongoDB. Do not need to cache it.
// mgo has an internal pool.
func (c *Config) getSession() (*mgo.Session, error) {

	session, err := mgo.Dial(c.Mongo.Host)
	if err != nil {
		return nil, err
	}

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	return session, nil
}

// Try to Get the MongoDB and yeah save it also.
func (c *Config) getDb(s *mgo.Session) (*mgo.Database, error) {
	db := s.DB(config.Mongo.DB)

	if len(config.Mongo.Username) == 0 {
		if len(config.Mongo.Password) == 0 {
			return db, nil
		}
		log.Fatalf("Username and Password should be provided to establish MongoDB connectivity")
	}

	if err := db.Login(config.Mongo.Username, config.Mongo.Password); err != nil {
		return nil, err
	}

	return db, nil
}

// GetClusterByID description
func GetClusterByID(hexID string) (c ClusterDesc) {
	// TODO move this const collection name to shared
	const DBColClusters = "Clusters"

	session, err := config.getSession()
	if err != nil {
		log.Fatalf("MongoDB connection failed: %s", err)
		return
	}
	defer session.Close()

	db, err := config.getDb(session)
	if err != nil {
		log.Fatalf("MongoDB session failed: %s", err)
		return
	}

	// err = db.C(DBColClusters).FindId(bson.ObjectIdHex(hexID).One(&c))

	// clusterCol := session.DB(types.DBStateDB).C(DBColClusters)
	// err := clusterCol.FindId(bson.ObjectIdHex(hexID)).One(&c)
	clusterCol := db.C(DBColClusters)
	err = clusterCol.FindId(bson.ObjectIdHex(hexID)).One(&c)

	if err != nil {
		log.Errorf("Failed to query cluster with id %s from DB: %s", hexID, err.Error())
	}

	return
}
