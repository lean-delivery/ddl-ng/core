package main

import (
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2/bson"
)

// Config is global configuration for worker
type Config struct {
	Daemon  daemon
	Mongo   mongo
	MQ      mq
	Logging logger
}

type daemon struct {
	Volume string `mapstructure:"volume"`
}

type mongo struct {
	Host     string `mapstructure:"host"`
	DB       string `mapstructure:"db"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
}

type mq struct {
	Host     string `mapstructure:"host"`
	Vhost    string `mapstructure:"vhost"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
}

type logger struct {
	Format    string `mapstructure:"format"`
	Level     string `mapstructure:"level"`
	Timestamp bool   `mapstructure:"timestamp"`
}

// ClusterDesc description fields in DB
type ClusterDesc struct {
	ObjID      bson.ObjectId `bson:"_id,omitempty"`
	User       string        `bson:"user"`
	Name       string        `bson:"name"`
	Type       string        `bson:"type"`
	PrivateKey string        `bson:"privateKey"`
	PublicKey  string        `bson:"publicKey"`
}

func stopSelf() {
	/*
	 * If we're in Docker container, then exiting the main
	 * process stops the whole thing
	 */
	log.Infof("Exiting")
	os.Exit(0)
}

func abortNotify() {
	log.Printf("My job should be aborted...")
}

// Mode - always docker
// TODO remove
var Mode string

// Global config
var config Config
var log = logrus.New()

func (config *Config) setLogging() {

	// inverse timestamp
	var dts bool
	if config.Logging.Timestamp {
		dts = false
	} else {
		dts = true
	}

	if config.Logging.Format == "" || config.Logging.Format == "text" {
		log.Formatter = &logrus.TextFormatter{FullTimestamp: true, DisableTimestamp: dts}
	} else {
		log.Formatter = &logrus.JSONFormatter{DisableTimestamp: dts}
	}

	switch config.Logging.Level {
	case "info":
		log.SetLevel(logrus.InfoLevel)
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "warn":
		log.SetLevel(logrus.WarnLevel)
	case "error":
		log.SetLevel(logrus.ErrorLevel)
	default:
		log.SetLevel(logrus.InfoLevel)
	}
	// file or stdout
	log.Out = os.Stdout
}

func main() {
	// Setup config
	viper.SetConfigName("worker")
	viper.AddConfigPath("/etc/ddl")
	viper.AddConfigPath(".")
	// Logging
	viper.SetDefault("logging.format", "text")
	viper.SetDefault("logging.level", "info")
	viper.SetDefault("logging.timestamp", true)
	// Mongo
	viper.SetDefault("mongo.host", "localhost:27017")
	viper.SetDefault("mongo.db", "ddl")
	viper.SetDefault("mongo.username", "mongouser")
	viper.SetDefault("mongo.password", "mongopass")
	// MQ
	viper.SetDefault("mq.host", "rabbitmq:5672")
	viper.SetDefault("mq.vhost", "ddl")
	viper.SetDefault("mq.username", "mquser")
	viper.SetDefault("mq.password", "mqpass")
	// Worker config
	viper.SetDefault("daemon.volume", "/var/ddl")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read config file: %v", err)
	}

	envVarPrefix := "worker"
	viper.SetEnvPrefix(envVarPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Failed to setup configuration: %v", err)
	}
	config.setLogging()

	log.Infof("Starting worker daemon")

	switch Mode {
	case "docker":
		abortHandler = stopSelf
	default:
		abortHandler = abortNotify
	}

	// just for test DB conncetion
	// session, err := config.getSession()
	// if err != nil {
	// 	log.Fatalf("MongoDB connection failed: %s", err)
	// 	return
	// }
	// defer session.Close()

	// _, err = config.getDb(session)
	// if err != nil {
	// 	log.Fatalf("MongoDB session failed: %s", err)
	// 	return
	// }
	// just for test DB conncetion

	mqStart()
}
