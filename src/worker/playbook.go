package main

import (
	"api"
	"bufio"
	"io"
	"os"
	"os/exec"
	"sync"
	"time"

	"github.com/streadway/amqp"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	clusterID   = "CLUSTER_ID="
	clusterType = "CLUSTER_TYPE="
	privateKey  = "PRIVATE_KEY="
	publicKey   = "PUBLIC_KEY="
)
const outCollectThresh = 8
const outCollectTmo = 4 * time.Second

func artifactsDir(msg *api.JobMsg) string {
	return api.ArtifactsDir(config.Daemon.Volume, msg.User, msg.JobID)
}

func forward(wg *sync.WaitGroup, w string, pipe io.ReadCloser, to chan *api.JobLogEntry) {
	defer wg.Done()

	log.Infof("Std%s started", w)

	scanner := bufio.NewScanner(pipe)
	for scanner.Scan() {
		txt := scanner.Text()
		to <- mkle(txt, w)
	}

	log.Infof("Std%s closed", w)
}

func mkle(txt, typ string) *api.JobLogEntry {
	return &api.JobLogEntry{
		Ts:   time.Now(),
		Type: typ,
		Text: txt,
	}
}

func flush(s *mgo.Session, jid string, text []*api.JobLogEntry) {
	/* FIXME -- saving logs in mongo might not be the best option */
	log.Printf("FIXME")
	session, err := config.getSession()
	if err != nil {
		log.Fatalf("MongoDB connection failed: %s", err)
		return
	}
	defer session.Close()

	db, err := config.getDb(session)
	if err != nil {
		log.Fatalf("MongoDB session failed: %s", err)
		return
	}
	const DBColJobs = "Jobs"
	err = db.C(DBColJobs).Update(bson.M{"_id": bson.ObjectIdHex(jid)},
		bson.M{"$push": bson.M{"logs": bson.M{"$each": text}}})

	if err != nil {
		log.Errorf("Error saving logs for %s: %s", jid, err.Error())
	}
	// err := s.DB(db.Dbname).C(types.DBColJobs).Update(bson.M{"_id": bson.ObjectIdHex(jid)},
	// 	bson.M{"$push": bson.M{"logs": bson.M{"$each": text}}})
}

func updateJobStatus(s *mgo.Session, jid string, status string) {
	log.Printf("FIXME")
	session, err := config.getSession()
	if err != nil {
		log.Fatalf("MongoDB connection failed: %s", err)
		return
	}
	defer session.Close()
	db, err := config.getDb(session)
	if err != nil {
		log.Fatalf("MongoDB session failed: %s", err)
		return
	}

	err = db.C(api.DBColJobs).Update(
		bson.M{
			"_id":    bson.ObjectIdHex(jid),
			"status": api.JobStarted,
		},
		bson.M{"$set": bson.M{
			"status": status,
			"finish": time.Now(),
		},
		})
	if err != nil && err != mgo.ErrNotFound {
		log.Errorf("Error updating status for %s: %s", jid, err.Error())
	}
}

func save(lch *amqp.Channel, s *mgo.Session, wg *sync.WaitGroup, jid string, sout, serr chan *api.JobLogEntry) {
	defer wg.Done()

	var seq int
	batch := []*api.JobLogEntry{}
	for {
		var le *api.JobLogEntry
		ff := false
		w := ""

		select {
		case <-time.After(outCollectTmo):
			ff = len(batch) != 0
			w = "tmo"
		case le = <-sout:
			w = "out"
		case le = <-serr:
			w = "err"
		}

		if w != "tmo" {
			if le == nil {
				if len(batch) != 0 {
					flush(s, jid, batch)
				}

				log.Infof("Done sending logs")
				return
			}

			le.Seq = seq
			seq++
			batch = append(batch, le)
			if lch != nil {
				mqPublishLog(lch, jid, le)
			}
			ff = len(batch) >= outCollectThresh
		}

		if ff {
			flush(s, jid, batch)
			batch = []*api.JobLogEntry{}
		}
	}
}

// RunPlaybook playbook execution
func runPlaybook(msg *api.JobMsg) {
	session, err := config.getSession()
	if err != nil {
		log.Fatalf("MongoDB session failed: %s", err)
		return
	}
	defer session.Close()

	lch, err := conn.Channel()
	if err != nil {
		log.Errorf("Failed to get amqp channel: %s", err.Error())
		lch = nil
	} else {
		defer lch.Close()
	}

	err = doRunPlaybook(session, lch, msg)

	/*
	 * First set the status, then send the fin. Message readers
	 * open the queue, then check the status
	 */
	if err == nil {
		updateJobStatus(session, msg.JobID, api.JobSucceeded)
		log.Infof("Run playbook succeeded")
	} else {
		updateJobStatus(session, msg.JobID, api.JobFailed)
		log.Errorf("Run playbook failed: %s", err.Error())
	}

	if lch != nil {
		mqPublishLog(lch, msg.JobID, mkle("", api.LogFinType))
	}
}

func doRunPlaybook(s *mgo.Session, lch *amqp.Channel, msg *api.JobMsg) error {
	pbroot := api.PlaybookDir(config.Daemon.Volume, msg.Playbook)
	cmd := exec.Command(pbroot + string(os.PathSeparator) + msg.Entrypoint)
	cmd.Dir = pbroot
	// cmd.Env = append(cmd.Env, "KEYS="+keyFile(msg))

	adir := artifactsDir(msg)

	clusterDesc := GetClusterByID(msg.ClusterId)

	cmd.Env = append(cmd.Env, clusterID+msg.ClusterId)
	cmd.Env = append(cmd.Env, clusterType+msg.Playbook)
	cmd.Env = append(cmd.Env, privateKey+clusterDesc.PrivateKey)
	cmd.Env = append(cmd.Env, publicKey+clusterDesc.PublicKey)

	err := os.MkdirAll(adir, 0750)
	if err != nil {
		log.Errorf("Failed to make [%s]: %s", adir, err.Error())
	}
	cmd.Env = append(cmd.Env, "ARTIFACTS="+adir)

	sout, _ := cmd.StdoutPipe()
	defer sout.Close()
	serr, _ := cmd.StderrPipe()
	defer serr.Close()

	err = cmd.Start()
	if err != nil {
		log.Errorf("Start playbook failed: %s", err.Error())
		return err
	}

	stdOutC := make(chan *api.JobLogEntry)
	stdErrC := make(chan *api.JobLogEntry)

	var fwg sync.WaitGroup
	fwg.Add(2)
	var swg sync.WaitGroup
	swg.Add(1)

	go forward(&fwg, "out", sout, stdOutC)
	go forward(&fwg, "err", serr, stdErrC)
	go save(lch, s, &swg, msg.JobID, stdOutC, stdErrC)

	fwg.Wait()
	log.Infof("Finished stdios forwarding")

	close(stdOutC)
	close(stdErrC)

	err = cmd.Wait()
	swg.Wait()

	return err
}
