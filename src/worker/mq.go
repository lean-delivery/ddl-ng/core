package main

import (
	"api"
	"encoding/json"

	"github.com/streadway/amqp"
)

var conn *amqp.Connection

func mqStart() {
	var err error
	conMQURL := config.MQ.Username + ":" + config.MQ.Password + "@" + config.MQ.Host + "/" + config.MQ.Vhost
	log.Infof("Starting mq listener %s@%s", config.MQ.Username, config.MQ.Host)
	conn, err = amqp.Dial("amqp://" + conMQURL)
	if err != nil {
		log.Errorf("error dial: %s", err.Error())
		return
	}

	err = setupLogs(conn)
	if err != nil {
		log.Errorf("error logs setup: %s", err.Error())
		return
	}

	stop := make(chan struct{})
	go processWorker(conn, stop)
	go processAborts(conn, stop)

	<-stop /* ANY exit matters */
	log.Infof("Exiting")
}

var runningJobID string
var abortHandler func()

func processWorker(conn *amqp.Connection, stop chan struct{}) {
	defer func() { stop <- struct{}{} }()

	channel, err := conn.Channel()
	if err != nil {
		log.Errorf("error get chan: %s", err.Error())
		return
	}

	q, err := channel.QueueDeclare(api.WorkerQueue, true, false, false, false, nil)
	if err != nil {
		log.Errorf("error queue declare: %s", err.Error())
		return
	}

	msgs, err := channel.Consume(q.Name, "", false, false, false, false, nil)
	if err != nil {
		log.Errorf("error consume: %s", err.Error())
		return
	}

	log.Infof("Getting job requests")
	for d := range msgs {
		log.Infof("mq: Received message [%s]", d.Body)

		var dm api.JobMsg

		err = json.Unmarshal(d.Body, &dm)
		if err != nil {
			log.Errorf("`- bad message: %s", err.Error())
			continue
		}

		runningJobID = dm.JobID
		log.Infof("Start %s job", dm.JobID)

		runPlaybook(&dm)

		runningJobID = ""
		if err := d.Ack(false); err != nil {
			log.Error("Error Submitting MQ message")
		}
	}

	log.Infof("Worker done")
}

func processAborts(conn *amqp.Connection, stop chan struct{}) {
	defer func() { stop <- struct{}{} }()

	channel, err := conn.Channel()
	if err != nil {
		log.Errorf("error get chan: %s", err.Error())
		return
	}

	err = channel.ExchangeDeclare(api.AbortExchange, "fanout", false, true, false, false, nil)
	if err != nil {
		log.Errorf("error get exchange: %s", err.Error())
		return
	}

	q, err := channel.QueueDeclare("", false, true, true, false, nil)
	if err != nil {
		log.Errorf("error queue declare: %s", err.Error())
		return
	}

	err = channel.QueueBind(q.Name, "", api.AbortExchange, false, nil)
	if err != nil {
		log.Errorf("error queue bind: %s", err.Error())
		return
	}

	msgs, err := channel.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Errorf("error consume: %s", err.Error())
		return
	}

	log.Infof("Getting aborts")
	for d := range msgs {
		log.Infof("mq: Received abort [%s]", d.Body)

		var dm api.AbortMsg

		err = json.Unmarshal(d.Body, &dm)
		if err != nil {
			log.Errorf("`- bad message: %s", err.Error())
			continue
		}

		if runningJobID == dm.JobID {
			log.Infof("Abort running %s job", dm.JobID)
			abortHandler()
		}
	}

	log.Infof("Aborts done")
}

func setupLogs(conn *amqp.Connection) error {
	channel, err := conn.Channel()
	if err != nil {
		log.Errorf("error get chan: %s", err.Error())
		return err
	}

	defer channel.Close()

	err = channel.ExchangeDeclare(api.LogsExchange, "direct", true, false, false, false, nil)
	if err != nil {
		log.Errorf("error get log exchange: %s", err.Error())
		return err
	}

	return nil
}

func mqPublishLog(ch *amqp.Channel, jid string, le *api.JobLogEntry) {
	data, _ := json.Marshal(le)
	err := ch.Publish(api.LogsExchange, jid, false, false, amqp.Publishing{
		ContentType: "application/json",
		Body:        data,
	})
	if err != nil {
		log.Warnf("Failed to send log: %s", err.Error())
	}
}
