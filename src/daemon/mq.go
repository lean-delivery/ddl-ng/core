package main

import (
	"api"
	"context"
	"encoding/json"
	"xrest"

	"github.com/streadway/amqp"
)

var conMQURL string

func mqPublishAnsibleWork(ctx context.Context, msg *api.JobMsg) *xrest.ReqErr {
	nConn, err := amqp.Dial("amqp://" + conMQURL)
	if err != nil {
		log.Errorf("Can't dial amqp: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot dial amqp")
	}

	defer nConn.Close()

	channel, err := nConn.Channel()
	if err != nil {
		log.Errorf("error get chan: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot open channel")
	}

	defer channel.Close()

	data, _ := json.Marshal(msg)
	err = channel.Publish("", api.WorkerQueue, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/json",
		Body:         data,
	})
	if err != nil {
		log.Errorf("Failed to send work notification: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot start worker")
	}

	return nil
}

func mqPublishAbort(ctx context.Context, msg *api.AbortMsg) *xrest.ReqErr {
	nConn, err := amqp.Dial("amqp://" + conMQURL)
	if err != nil {
		log.Errorf("Can't dial amqp: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot dial amqp")
	}

	defer nConn.Close()

	channel, err := nConn.Channel()
	if err != nil {
		log.Printf("error get chan: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot open channel")
	}

	defer channel.Close()

	data, _ := json.Marshal(msg)
	err = channel.Publish(api.AbortExchange, "", false, false, amqp.Publishing{
		ContentType: "application/json",
		Body:        data,
	})
	if err != nil {
		log.Errorf("Failed to send abort notification: %s", err.Error())
		return DErrM(xrest.GenErr, "Cannot stop worker")
	}

	return nil
}

func mqConnect() error {
	conMQURL = config.MQ.Username + ":" + config.MQ.Password + "@" + config.MQ.Host + "/" + config.MQ.Vhost

	log.Printf("Connect to mq %s@%s", config.MQ.Username, config.MQ.Host)
	nConn, err := amqp.Dial("amqp://" + conMQURL)
	if err != nil {
		log.Errorf("Can't dial amqp: %s", err.Error())
		return err
	}

	defer nConn.Close()

	nChan, err := nConn.Channel()
	if err != nil {
		nConn.Close()
		log.Errorf("Can't get channel: %s", err.Error())
		return err
	}

	defer nChan.Close()

	_, err = nChan.QueueDeclare(api.WorkerQueue, true, false, false, false, nil)
	if err != nil {
		log.Errorf("error queue declare: %s", err.Error())
		return err
	}

	err = nChan.ExchangeDeclare(api.AbortExchange, "fanout", false, true, false, false, nil)
	if err != nil {
		log.Errorf("error get exchange: %s", err.Error())
		return err
	}

	return nil
}

func receiveLogs(jid string, to chan *api.JobLogEntry, stop chan struct{}) error {
	nConn, err := amqp.Dial("amqp://" + conMQURL)
	if err != nil {
		log.Errorf("Can't dial amqp: %s", err.Error())
		return err
	}

	defer nConn.Close()

	channel, err := nConn.Channel()
	if err != nil {
		log.Errorf("error get chan: %s", err.Error())
		return err
	}

	q, err := channel.QueueDeclare("", false, true, true, false, nil)
	if err != nil {
		log.Errorf("error queue declare: %s", err.Error())
		return err
	}

	err = channel.QueueBind(q.Name, jid, api.LogsExchange, false, nil)
	if err != nil {
		log.Errorf("error queue bind: %s", err.Error())
		return err
	}

	msgs, err := channel.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		log.Errorf("error consume: %s", err.Error())
		return err
	}

	go func() {
		defer channel.Close()
		defer close(to)

		for {
			select {
			case d := <-msgs:
				dle := &api.JobLogEntry{}
				if err := json.Unmarshal(d.Body, dle); err != nil{
					log.Errorf("Error Unmarshalling JSON string: %v", err)
				}
				to <- dle
				if dle.Type == api.LogFinType {
					log.Printf("Stop getting messages for %s (%s)", jid, q.Name)
					return
				}
			case <-stop:
				log.Printf("Abort getting messages for %s (%s)", jid, q.Name)
				return
			}
		}
	}()

	return nil
}
