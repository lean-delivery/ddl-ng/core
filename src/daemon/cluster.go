package main

import (
	"api"
	"context"
	"net/http"
	"net/url"
	"xrest"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type ClusterDesc struct {
	ObjID       bson.ObjectId                 `bson:"_id,omitempty"`
	User        string                        `bson:"user"`
	Name        string                        `bson:"name"`
	Type        string                        `bson:"type"`
	PrivateKey  string                        `bson:"privateKey"`
	PublicKey   string                        `bson:"publicKey"`
	Description []*api.ClusterDescriptionInfo `bson:"description"`
}

func (cl *ClusterDesc) Add(ctx context.Context, p interface{}) *xrest.ReqErr {
	cl.ObjID = bson.NewObjectId()

	err := dbInsert(ctx, cl)
	if err != nil {
		log.Errorf("Error Inserting cluster %s into DB: %v", cl.ObjID.Hex(), err)
		return DErrD(err)
	}

	return nil
}

func (cl *ClusterDesc) Upd(ctx context.Context, upd interface{}) *xrest.ReqErr {
	log.Warn("Cluster is not updatable entity")
	return DErrM(xrest.GenErr, "Not updatable")
}

func (cl *ClusterDesc) Del(ctx context.Context, p interface{}) *xrest.ReqErr {
	if p.(*api.ClusterDelById).DoRemoveDbEntry {
		hack := true
		log.Infof("delete %s will be deleted", cl.Name)
		if hack {
			err := dbRemove(ctx, cl)
			if err != nil {
				log.Errorf("Error removing cluster %s from DB: %v", cl.ObjID.Hex(), err)
				return DErrD(err)
			}
		}
	}

	jd := JobDesc{
		ObjID:      bson.NewObjectId(),
		Cluster:    cl.ObjID,
		User:       cl.User,
		Playbook:   cl.Type,
		Entrypoint: "destroy.sh",
	}

	return jd.Add(ctx, nil)

}

func (cl *ClusterDesc) lastJob(ctx context.Context) (*JobDesc, error) {
	var ld JobDesc

	err := dbFindJob(ctx, bson.M{"cluster_id": cl.ObjID}).Sort("-started").Limit(1).One(&ld)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = nil
		} else {
			log.Errorf("Error Selecting started cluster %s from DB: %v", cl.ObjID.Hex(), err)
		}

		return nil, err
	}

	return &ld, nil
}

func (cl *ClusterDesc) Info(ctx context.Context, q url.Values, details bool) (interface{}, *xrest.ReqErr) {
	ci := &api.ClusterInfo{
		ID:   cl.ObjID.Hex(),
		Name: cl.Name,
	}

	ld, err := cl.lastJob(ctx)
	if err != nil {
		//we already logged corresponding error inside of lastJob method
		return nil, DErrD(err)
	}

	if ld != nil {
		ldi, cerr := ld.Info(ctx, q, details)
		if cerr != nil {
			return nil, cerr
		}

		ci.LD = ldi.(*api.ClusterJobInfo)
	}

	// if details {
	// 	for nn, nd := range cl.Nodes {
	// 		ni, _ := nd.info(ctx, cl, nn, details)
	// 		ci.Nodes = append(ci.Nodes, ni.(*api.NodeInfo))
	// 	}
	// }

	return ci, nil
}

func (cl *ClusterDesc) Keygen(ctx context.Context) *xrest.ReqErr {
	privateKey, publicKey, err := sshKeygen(ctx)
	if err != nil {
		log.Errorf("Error Generating keys for cluster %s: %v", cl.ObjID.Hex(), err)
		return DErrM(xrest.GenErr, "Cannot make keys")
	}

	cl.PrivateKey = privateKey
	cl.PublicKey = publicKey

	err = dbUpdatePart(ctx, cl, bson.M{"privateKey": privateKey})
	if err != nil {
		log.Errorf("Error updating cluster %s with generated private key: %v", cl.ObjID.Hex(), err)
		return DErrD(err)
	}

	err = dbUpdatePart(ctx, cl, bson.M{"publicKey": publicKey})
	if err != nil {
		log.Errorf("Error updating cluster %s with generated public key: %v", cl.ObjID.Hex(), err)
		return DErrD(err)
	}

	return nil
}

type Clusters struct{}

func (_ Clusters) Create(ctx context.Context, p interface{}) (xrest.Obj, *xrest.ReqErr) {
	params := p.(*api.ClusterAdd)
	return &ClusterDesc{
		User: gctx(ctx).User,
		Name: params.Name,
		Type: params.Type,
	}, nil
}

func (_ Clusters) Get(ctx context.Context, r *http.Request) (xrest.Obj, *xrest.ReqErr) {
	var cl ClusterDesc

	cerr := objFindForReq(ctx, r, "cid", &cl)
	if cerr != nil {
		return nil, cerr
	}

	return &cl, nil
}

func (_ Clusters) Iterate(ctx context.Context, q url.Values, cb func(context.Context, xrest.Obj) *xrest.ReqErr) *xrest.ReqErr {
	cname := q.Get("name")

	var cl ClusterDesc

	if cname != "" {
		err := dbFind(ctx, nameReq(ctx, cname), &cl).One(&cl)
		if err != nil {
			log.Errorf("Error Fetching clusters from the DB: %v", err)
			return DErrD(err)
		}

		return cb(ctx, &cl)
	}

	iter := dbFind(ctx, listReq(ctx, q["label"]), &cl).Iter()
	defer iter.Close()

	for iter.Next(&cl) {
		cerr := cb(ctx, &cl)
		if cerr != nil {
			log.Errorf("Error Iterating through clusters: %v", cerr)
			return cerr
		}
	}

	err := iter.Err()
	if err != nil {
		log.Errorf("Error Iterating through clusters: %v", err)
		return DErrD(err)
	}

	return nil
}

type ClKeyProp struct{}

func (_ ClKeyProp) Info(ctx context.Context, o xrest.Obj, q url.Values) (interface{}, *xrest.ReqErr) {
	cl := o.(*ClusterDesc)
	// TODO: add error handling
	if cl.PrivateKey == "" {
		log.Warn("No Public key generated/stored")
		return nil, DErrM(api.NotFound, "No Public key generated/stored")
	}
	if cl.PrivateKey == "" {
		log.Warn("No Private key generated/stored")
		return nil, DErrM(api.NotFound, "No Private key generated/stored")
	}
	pub := cl.PrivateKey

	return &api.ClusterKeyInfo{Key: pub}, nil
}

func (_ ClKeyProp) Upd(ctx context.Context, o xrest.Obj, u interface{}) *xrest.ReqErr {
	log.Warn("Cluster key prop is not updatable entity")
	return DErrM(xrest.GenErr, "Not updatable")
}

type ClDescriptionProp struct{}

func (_ ClDescriptionProp) Info(ctx context.Context, o xrest.Obj, q url.Values) (interface{}, *xrest.ReqErr) {
	cl := o.(*ClusterDesc)
	return &cl.Description, nil
}

func (_ ClDescriptionProp) Upd(ctx context.Context, o xrest.Obj, u interface{}) *xrest.ReqErr {
	return DErrM(xrest.GenErr, "Not updatable")
}
