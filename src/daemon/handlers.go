package main

import (
	"api"
	"context"
	"encoding/json"
	"net/http"
	"xrest"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

func handleClusters(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	var params api.ClusterAdd
	return xrest.HandleMany(ctx, w, r, Clusters{}, &params)
}

func handleCluster(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	var params api.ClusterDelById
	return xrest.HandleOne(ctx, w, r, Clusters{}, &params)
}

func handleClusterDescription(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	var params api.ClusterDescriptionInfo
	return xrest.HandleProp(ctx, w, r, Clusters{}, &ClDescriptionProp{}, params)
}

func handleClusterSSHKey(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	return xrest.HandleProp(ctx, w, r, Clusters{}, &ClKeyProp{}, nil)
}

func handleClusterSSHKeyGen(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	x, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	cl := x.(*ClusterDesc)
	return cl.Keygen(ctx)
}

func handleJobs(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	var da api.ClusterJob
	return xrest.HandleMany(ctx, w, r, Jobs{cl.(*ClusterDesc)}, &da)
}

func handleJob(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	return xrest.HandleOne(ctx, w, r, Jobs{cl.(*ClusterDesc)}, nil)
}

func handleJobLogs(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	return xrest.HandleProp(ctx, w, r, Jobs{cl.(*ClusterDesc)}, JobLogsProp{}, nil)
}

var wsupgrader = websocket.Upgrader{}

func handleJobLogsPull(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	log.Printf("Request for logs pull: %s", r.URL.Path)

	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	did := mux.Vars(r)["did"]
	les := make(chan *api.JobLogEntry)
	stop := make(chan struct{})
	defer close(stop)

	err := receiveLogs(did, les, stop)
	if err != nil {
		return DErrM(xrest.GenErr, "Cannot pull logs")
	}

	d, cerr := Jobs{cl.(*ClusterDesc)}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	jobDesc := d.(*JobDesc)
	c, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		return DErrM(xrest.GenErr, "Cannot make websocket")
	}

	cls := make(chan struct{})
	go func() {
		/* XXX SetCloseHandler() doesn't work */
		for {
			_, _, err := c.ReadMessage()
			if err == nil {
				continue
			}
			log.Printf("Client closes connection")
			close(cls)
			break
		}
	}()

	defer c.Close()

	/*
	 * If the sattus is not started we'll see nothing in mq and stuck.
	 * if not, the worker hasn't pushed the "fin" message before we
	 * subscribed to it.
	 */
	if jobDesc.Status == api.JobStarted {
		for {
			select {
			case msg := <-les:
				if msg.Type == api.LogFinType {
					goto out
				}

				data, _ := json.Marshal(msg)
				if err := c.WriteMessage(websocket.BinaryMessage, data); err != nil {
					log.Errorf("Error writing the response: %s", err.Error())
				}
			case <-cls:
				goto out
			}
		}
	}

out:
	c.WriteMessage(websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	return nil
}

func handleArtifacts(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	return xrest.HandleProp(ctx, w, r, Jobs{cl.(*ClusterDesc)}, JobArtifactsProp{}, nil)
}

func handleArtifact(ctx context.Context, w http.ResponseWriter, r *http.Request) *xrest.ReqErr {
	cl, cerr := Clusters{}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	job, cerr := Jobs{cl.(*ClusterDesc)}.Get(ctx, r)
	if cerr != nil {
		return cerr
	}

	data, cerr := job.(*JobDesc).Artifact(ctx, mux.Vars(r)["aname"])
	if cerr != nil {
		return cerr
	}

	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write(data); err != nil {
		log.Errorf("Error Writing response: %v", err)
	}

	return nil
}
