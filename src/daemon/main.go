package main

import (
	"context"
	"net/http"
	"os"
	"strings"
	"time"
	"xhttp"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/mgo.v2"
)

// Config is global configuration for worker
type Config struct {
	Daemon  daemon
	Mongo   mongo
	MQ      mq
	Logging logger
}

type daemon struct {
	Volume  string `mapstructure:"volume"`
	JWTK    string `mapstructure:"jwtk"`
	Address string `mapstructure:"address"`
	HTTPS   bool   `mapstructure:"https"`
	Cert    string `mapstructure:"cert"`
	Key     string `mapstructure:"key"`
}

type mongo struct {
	Host     string `mapstructure:"host"`
	DB       string `mapstructure:"db"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
}

type mq struct {
	Host     string `mapstructure:"host"`
	Vhost    string `mapstructure:"vhost"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
}

type logger struct {
	Format    string `mapstructure:"format"`
	Level     string `mapstructure:"level"`
	Timestamp bool   `mapstructure:"timestamp"`
}

// Global config
var config Config
var log = logrus.New()

func initConfig() {
	// Setup config
	viper.SetConfigName("daemon")
	viper.AddConfigPath("/etc/ddl")
	viper.AddConfigPath(".")
	// Logging
	viper.SetDefault("logging.format", "text")
	viper.SetDefault("logging.level", "info")
	viper.SetDefault("logging.timestamp", true)
	// Mongo
	viper.SetDefault("mongo.host", "localhost:27017")
	viper.SetDefault("mongo.db", "ddl")
	viper.SetDefault("mongo.username", "mongouser")
	viper.SetDefault("mongo.password", "mongopass")
	// MQ
	viper.SetDefault("mq.host", "rabbitmq:5672")
	viper.SetDefault("mq.vhost", "ddl")
	viper.SetDefault("mq.username", "mquser")
	viper.SetDefault("mq.password", "mqpass")
	// Worker config
	viper.SetDefault("daemon.volume", "/var/ddl")
	viper.SetDefault("daemon.jwtk", "64379045bdc723149d016718")
	viper.SetDefault("daemon.port", "0.0.0.0")
	viper.SetDefault("daemon.address", "8681")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read config file: %v", err)
	}

	const envVarPrefix = "daemon"
	viper.SetEnvPrefix(envVarPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Failed to setup configuration: %v", err)
	}
	config.setLogging()
}

func (config *Config) setLogging() {

	// inverse timestamp
	var dts bool
	if config.Logging.Timestamp {
		dts = false
	} else {
		dts = true
	}

	if config.Logging.Format == "" || config.Logging.Format == "text" {
		log.Formatter = &logrus.TextFormatter{FullTimestamp: true, DisableTimestamp: dts}
	} else {
		log.Formatter = &logrus.JSONFormatter{DisableTimestamp: dts}
	}

	switch config.Logging.Level {
	case "info":
		log.SetLevel(logrus.InfoLevel)
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "warn":
		log.SetLevel(logrus.WarnLevel)
	case "error":
		log.SetLevel(logrus.ErrorLevel)
	default:
		log.SetLevel(logrus.InfoLevel)
	}
	// file or stdout
	log.Out = os.Stdout
}

type dContext struct {
	context.Context
	User string
	S    *mgo.Session
}

func gctx(ctx context.Context) *dContext {
	return ctx.(*dContext)
}

func main() {
	initConfig()

	err := authInit()
	if err != nil {
		log.Printf("Cannot init auth: %s", err.Error())
		return
	}

	err = dbConnect()
	if err != nil {
		log.Printf("Cannot connect to DB: %s", err.Error())
		return
	}

	err = mqConnect()
	if err != nil {
		log.Printf("Cannot connect to amqp: %s", err.Error())
		return
	}

	r := mux.NewRouter()
	r.HandleFunc("/v1/login", handleUserLogin).Methods("POST", "OPTIONS")
	r.Handle("/v1/clusters", authHandler(handleClusters)).Methods("GET", "POST", "OPTIONS")
	r.Handle("/v1/clusters/{cid}", authHandler(handleCluster)).Methods("GET", "DELETE", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/description", authHandler(handleClusterDescription)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/sshkey", authHandler(handleClusterSSHKey)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/sshkey/make", authHandler(handleClusterSSHKeyGen)).Methods("POST", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/jobs", authHandler(handleJobs)).Methods("GET", "POST", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/jobs/{did}", authHandler(handleJob)).Methods("GET", "DELETE", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/jobs/{did}/logs", authHandler(handleJobLogs)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/jobs/{did}/logs/pull", authHandler(handleJobLogsPull)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/jobs/{did}/artifacts", authHandler(handleArtifacts)).Methods("GET", "OPTIONS")
	r.Handle("/v1/clusters/{cid}/jobs/{did}/artifacts/{aname}", authHandler(handleArtifact)).Methods("GET", "OPTIONS")

	log.Printf("Starting http server %s", config.Daemon.Address)
	err = xhttp.ListenAndServe(
		&http.Server{
			Handler:      r,
			Addr:         config.Daemon.Address,
			WriteTimeout: 60 * time.Second,
			ReadTimeout:  60 * time.Second,
		},
		config.Daemon.HTTPS,
		config.Daemon.Cert,
		config.Daemon.Key, func(s string) { log.Printf(s) })
	if err != nil {
		log.Printf("ListenAndServe error: %s", err.Error())
	}
}
