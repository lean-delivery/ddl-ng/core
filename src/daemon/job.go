package main

import (
	"api"
	"context"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strconv"
	"time"
	"xrest"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Jobs struct{ cl *ClusterDesc }

type JobDesc struct {
	ObjID      bson.ObjectId      `bson:"_id,omitempty"`
	Started    time.Time          `bson:"started"`
	Cluster    bson.ObjectId      `bson:"cluster_id"`
	User       string             `bson:"user"`
	Playbook   string             `bson:"playbook"`
	Status     string             `bson:"status"`
	Finished   *time.Time         `bson:"finished,omitempty"`
	Entrypoint string             `bson:"entrypoint"`
	Logs       []*api.JobLogEntry `bson:"logs"`

	/* These are on-time parameter(s), not to be kept in DB */
	keys  string `bson:"-"`
	hosts string `bson:"-"`
}

func (ds Jobs) Create(ctx context.Context, p interface{}) (xrest.Obj, *xrest.ReqErr) {
	cl := ds.cl

	if cl.PrivateKey == "" {
		log.Errorf("No private key for job %s found", ds.cl.ObjID.Hex())
		return nil, DErrM(api.NotAvailable, "No private key for this job found")
	}
	if cl.PublicKey == "" {
		log.Errorf("No public key for job %s found", ds.cl.ObjID.Hex())
		return nil, DErrM(api.NotAvailable, "No public key for this job found")
	}

	// Job have no parameters
	// params := p.(*api.ClusterJob)

	dd := &JobDesc{}

	dd.ObjID = bson.NewObjectId()
	dd.Cluster = cl.ObjID
	dd.User = cl.User
	dd.Playbook = cl.Type
	// TDOD add remove job into /clusters/${cid}/jobs/${did}
	dd.Entrypoint = "run.sh"

	return dd, nil
}

func dbFindJob(ctx context.Context, q bson.M) *mgo.Query {
	return dbFind(ctx, q, &JobDesc{}).Select(bson.M{"logs": 0})
}

func (ds Jobs) Get(ctx context.Context, r *http.Request) (xrest.Obj, *xrest.ReqErr) {
	did := mux.Vars(r)["did"]
	if !bson.IsObjectIdHex(did) {
		log.Errorf("Bad job ID: %s", did)
		return nil, DErrM(xrest.BadRequest, "Bad job ID")
	}

	var dd JobDesc

	err := dbFindJob(ctx, bson.M{"_id": bson.ObjectIdHex(did), "cluster_id": ds.cl.ObjID}).One(&dd)
	if err != nil {
		log.Errorf("Error Getting corresponding cluster by id %s: %v", did, err)
		return nil, DErrD(err)
	}

	return &dd, nil
}

func (ds Jobs) Iterate(ctx context.Context, q url.Values, cb func(context.Context, xrest.Obj) *xrest.ReqErr) *xrest.ReqErr {
	var dd JobDesc

	iter := dbFindJob(ctx, bson.M{"cluster_id": ds.cl.ObjID}).Iter()
	defer iter.Close()

	for iter.Next(&dd) {
		cerr := cb(ctx, &dd)
		if cerr != nil {
			log.Errorf("Error Iterating through job entries: %v", cerr)
			return cerr
		}
	}

	err := iter.Err()
	if err != nil {
		log.Errorf("Error Iterating through job entries: %v", err)
		return DErrD(err)
	}

	return nil
}

func (dd *JobDesc) amqp() *api.JobMsg {
	return &api.JobMsg{
		JobID:      dd.ObjID.Hex(),
		User:       dd.User,
		Playbook:   dd.Playbook,
		Entrypoint: dd.Entrypoint,
		ClusterId:  dd.Cluster.Hex(),
	}
}

func (dd *JobDesc) Info(ctx context.Context, q url.Values, details bool) (interface{}, *xrest.ReqErr) {
	di := &api.ClusterJobInfo{
		Id:       dd.ObjID.Hex(),
		Playbook: dd.Playbook,
		Started:  dd.Started.Format(time.RFC1123Z),
		Status:   dd.Status,
	}

	if dd.Finished != nil {
		di.Finished = dd.Finished.Format(time.RFC1123Z)
	}

	return di, nil
}

func (dd *JobDesc) Add(ctx context.Context, _ interface{}) *xrest.ReqErr {
	dd.Started = time.Now()
	dd.Status = api.JobStarted
	path := api.PlaybookDir(config.Daemon.Volume, dd.Playbook)

	st, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			log.Errorf("Runbook is not found in: %s", path)
			return DErrM(api.NotFound, "No such runbook")
		} else {
			log.Errorf("Error in playbook area: %s", err.Error())
			return DErrM(xrest.GenErr, "Error accessing playbook")
		}
	} else if !st.IsDir() {
		log.Errorf("Error in playbook area for %s", dd.Playbook)
		return DErrM(xrest.GenErr, "Error accessing playbook")
	}

	err = dbInsert(ctx, dd)
	if err != nil {
		log.Errorf("Error Inserting to DB: %v", err)
		return DErrD(err)
	}

	return mqPublishAnsibleWork(ctx, dd.amqp())
}

func (dd *JobDesc) Upd(ctx context.Context, _ interface{}) *xrest.ReqErr {
	log.Warn("Job is not updatable entity")
	return DErrM(xrest.GenErr, "Not updatable")
}

func (dd *JobDesc) Del(ctx context.Context, _ interface{}) *xrest.ReqErr {
	cerr := mqPublishAbort(ctx, &api.AbortMsg{JobID: dd.ObjID.Hex()})
	if cerr != nil {
		log.Errorf("Error deleting job %s: %v", dd.ObjID.Hex(), cerr)
		return cerr
	}

	adir := api.ArtifactsDir(config.Daemon.Volume, gctx(ctx).User, dd.ObjID.Hex())
	err := os.Rename(adir, adir+".dead")
	if err == nil {
		go func() {
			err = os.RemoveAll(adir + ".dead")
			if err != nil {
				log.Errorf("Leak %s: %s", adir+".dead", err.Error())
			}
		}()
	} else if !os.IsNotExist(err) {
		log.Errorf("Cannot remove artifacts %s: %s", adir, err.Error())
		return DErrM(xrest.GenErr, "Cannot remove artifacts")
	}

	err = dbRemove(ctx, dd)
	if err != nil {
		log.Errorf("Error Removing job %s from DB: %v", dd.ObjID.Hex(), err)
		return DErrD(err)
	}

	return nil
}

type JobLogsProp struct{}

func (_ JobLogsProp) Info(ctx context.Context, o xrest.Obj, q url.Values) (interface{}, *xrest.ReqErr) {
	job := o.(*JobDesc)
	var job_full JobDesc

	dbq := dbFind(ctx, bson.M{"_id": job.ObjID}, &job_full)

	off := q.Get("skip")
	if off != "" {
		off_i, err := strconv.Atoi(off)
		if err != nil {
			log.Errorf("Error converting skip value %s to int: %v", off, err)
			return nil, DErrM(xrest.BadRequest, "The skip value should be int'")
		}

		skip_slice := []int{off_i, 1000}
		dbq = dbq.Select(bson.M{"logs": bson.M{"$slice": skip_slice}})
	}

	err := dbq.One(&job_full)
	if err != nil {
		log.Errorf("Error Fetching job logs from DB: %v", err)
		return nil, DErrD(err)
	}

	logs := job_full.Logs
	sort.SliceStable(logs, func(i, j int) bool { return logs[i].Ts.Before(logs[j].Ts) })

	ret := []*api.LogRecord{}
	for _, le := range logs {
		ret = append(ret, &api.LogRecord{
			Ts:   le.Ts.Format(time.RFC1123Z),
			Type: le.Type,
			Text: le.Text,
		})
	}

	return ret, nil
}

func (dlp JobLogsProp) Upd(ctx context.Context, o xrest.Obj, p interface{}) *xrest.ReqErr {
	log.Warn("Job logs is not updatable entity")
	return DErrM(xrest.GenErr, "Not updatable")
}

type JobArtifactsProp struct{}

func (JobArtifactsProp) Info(ctx context.Context, o xrest.Obj, q url.Values) (interface{}, *xrest.ReqErr) {
	job := o.(*JobDesc)

	adir := api.ArtifactsDir(config.Daemon.Volume, gctx(ctx).User, job.ObjID.Hex())
	d, err := os.Open(adir)
	if err != nil {
		log.Errorf("Error fetching artifacts for job %s", job.ObjID.Hex())
		return nil, DErrM(xrest.GenErr, "Cannot find artifacts")
	}

	defer d.Close()
	afiles, err := d.Readdirnames(-1)
	if err != nil {
		log.Errorf("Error fetching artifacts for job %s", job.ObjID.Hex())
		return nil, DErrM(xrest.GenErr, "Cannot find artifacts")
	}

	return afiles, nil
}

func (_ JobArtifactsProp) Upd(ctx context.Context, o xrest.Obj, p interface{}) *xrest.ReqErr {
	log.Warn("Job artifacts is not updatable entity")
	return DErrM(xrest.GenErr, "Not updatable")
}

func (job *JobDesc) Artifact(ctx context.Context, aname string) ([]byte, *xrest.ReqErr) {
	adir := api.ArtifactsDir(config.Daemon.Volume, gctx(ctx).User, job.ObjID.Hex())
	data, err := ioutil.ReadFile(adir + "/" + aname)
	if err != nil {
		if os.IsNotExist(err) {
			log.Errorf("Error fetching artifacts for job %s", job.ObjID.Hex())
			return nil, DErrM(api.NotFound, "No such artifact")
		} else {
			log.Errorf("Error reading artifacts for job %s", job.ObjID.Hex())
			return nil, DErrM(xrest.GenErr, "Error reading artifact")
		}
	}

	return data, nil
}
